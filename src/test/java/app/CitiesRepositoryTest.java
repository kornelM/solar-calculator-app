package app;

import com.solarcalculator.app.model.components.City;
import com.solarcalculator.app.repositories.CitiesRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Created by kornel on 07.07.17.
 */
public class CitiesRepositoryTest {

    private CitiesRepository citiesRepo;

    @Before
    public void setUp() {
        citiesRepo = new CitiesRepository();
    }


    @Test
    public void shouldReturnCityWhenPassedItsName(){
        City expectedCity = new City("Kraków", 1044, 1470);

        String nameOfCity = "Kraków";
        City actualCity = citiesRepo.getCityByName(nameOfCity);

        Assert.assertNotNull(actualCity);
        Assert.assertEquals(expectedCity.getName(), actualCity.getName());
        Assert.assertEquals(expectedCity.getInsolation(), actualCity.getInsolation());
        Assert.assertEquals(expectedCity.getSunnyHours(), actualCity.getSunnyHours());
    }

}