package com.solarcalculator.app.model.calculations;

import com.solarcalculator.app.model.PvInfoFromUser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.*;

/**
 * Created by kornel on 07.07.17.
 */
public class EnergyCounterTest {
    private EnergyCounter energyCounter;


    @Before
    public void setUp(){
        PvInfoFromUser pvInfoFromUser = new PvInfoFromUser(4000, 200, 13, "Kraków", 9,100, "Dach płaski");
        energyCounter = new EnergyCounter(pvInfoFromUser);
    }


    @Test
    public void shouldReturnEnergyDemandThenUserSendsInfo(){
        BigDecimal expected =  BigDecimal.valueOf(4.62); //actually it is something more than 4.48 but this precision is enough

        BigDecimal actual = energyCounter.countEnergyDemand();

        Assert.assertEquals(expected, actual);

    }

}