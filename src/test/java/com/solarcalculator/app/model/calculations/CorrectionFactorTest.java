package com.solarcalculator.app.model.calculations;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by kornel on 07.07.17.
 */
public class CorrectionFactorTest {

    private CorrectionFactor correctionFactor;


    @Before
    public void setUp() {
        correctionFactor = new CorrectionFactor();
    }


    @Test
    public void shouldReturnCorrectionFactorWhenRoofAngleAndDeviationFromSouthWerePassed() {
        CorrectionFactor correctionFactor = new CorrectionFactor();
        Double expectedCorrectionFactor1 = 0.93;

        //roof angle = 65deg (5), deviation from south = 60deg (12)
        Double actualCorrectionFactor1 = correctionFactor.getCorrectionFactor(12, 5);

        Assert.assertEquals(expectedCorrectionFactor1, actualCorrectionFactor1);

        Double expectedCorrectionFactor2 = 0.77;
        //roof angle = 90deg (18), deviation from south = 45deg (9)
        Double actualCorrectionFactor2 = correctionFactor.getCorrectionFactor(18, 9);
        Assert.assertEquals(expectedCorrectionFactor2, actualCorrectionFactor2);

        Double expectedCorrectionFactor3 = 1.13;
        //roof angle = 30deg (6), deviation from south = 0deg (18)
        Double actualCorrectionFactor3 = correctionFactor.getCorrectionFactor(6, 18);
        Assert.assertEquals(expectedCorrectionFactor3, actualCorrectionFactor3);


    }

}