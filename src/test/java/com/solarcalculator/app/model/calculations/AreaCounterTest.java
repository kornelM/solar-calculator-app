package com.solarcalculator.app.model.calculations;

import com.solarcalculator.app.model.PvInfoFromUser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by kornel on 13.07.17.
 */
public class AreaCounterTest {

    private AreaCounter areaCounter;
    private PvInfoFromUser pvInfoFromUser;
    private EnergyCounter energyCounter;


    @Before
    public void setUp(){
        pvInfoFromUser = new PvInfoFromUser(4000, 200, 13, "Kraków",
                9,100, "Dach skośny – dachówka");
        energyCounter = new EnergyCounter(pvInfoFromUser);
        areaCounter = new AreaCounter(pvInfoFromUser, energyCounter.countEnergyDemand());
    }



    @Test
    public void shouldReturnNameOfPanelWhenGivenDataFromAbove() throws Exception {
        String expected = "SE 250/60P";
                String actual = areaCounter.getName();

        Assert.assertEquals(expected, actual);
    }

    @Test
    public void shouldReturnNameOfStrongerPanelBecauseRoofIsSmaller() throws Exception {
        pvInfoFromUser = new PvInfoFromUser(4000, 200, 13, "Kraków",
                9,22, "Dach skośny – dachówka");
        energyCounter = new EnergyCounter(pvInfoFromUser);
        areaCounter = new AreaCounter(pvInfoFromUser, energyCounter.countEnergyDemand());

        String expected = "SE 310/72P";
        String actual = areaCounter.getName();

        Assert.assertEquals(expected, actual);
    }



    @Test
    public void shouldReturnNeededRoofArea(){
        double expectedAreaDemand = 27.2;
        double actualAreaDemand = Double.valueOf(String.valueOf(areaCounter.getNeededArea()));

        Assert.assertEquals(expectedAreaDemand, actualAreaDemand, 0.01);
    }

}