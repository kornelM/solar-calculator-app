package com.solarcalculator.app.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kornel on 06.07.17.
 */
public class PvInfoFromUser {
    private int kwh;    // average annual usage of energy
    private int bill;   // average annual bill for electricity
    private int roofAngle;  //angle of roof
    private String city;    // name of city (because of amount of energy from Sun)
    private int deviationFromSouth;  //
    private int roofArea;
    private int deviationFromSouthPercent;
    private int roofAnglePercent;
    private String roofType;


    //getters are used by Themeleaf to get values

    public PvInfoFromUser(int kwh, int bill, int roofAngle, String city, int deviationFromSouth, int roofArea, String roofType) {
        this.kwh = kwh;
        this.bill = bill;
        this.roofAngle = roofAngle;
        this.city = city;
        this.deviationFromSouth = deviationFromSouth;
        this.roofArea = roofArea;
        this.roofType = roofType;

        setDeviationFromSouthPercent(deviationFromSouth);
        setRoofAnglePercent(roofAngle);
    }

    private void setDeviationFromSouthPercent(int number) {
        Map<Integer, Integer> percentsMap = new HashMap<Integer, Integer>();
        percentsMap.put(18, 0);
        percentsMap.put(17, 5);
        percentsMap.put(16,10);
        percentsMap.put(15,15);
        percentsMap.put(14,20);
        percentsMap.put(13,25);
        percentsMap.put(12,30);
        percentsMap.put(11,35);
        percentsMap.put(10,40);
        percentsMap.put( 9,45);
        percentsMap.put( 8,50);
        percentsMap.put( 7,55);
        percentsMap.put( 6,60);
        percentsMap.put( 5,65);
        percentsMap.put( 4,70);
        percentsMap.put( 3,75);
        percentsMap.put( 2,80);
        percentsMap.put( 1,85);
        percentsMap.put( 0,90);

        deviationFromSouthPercent = percentsMap.get(number);

        /* html form from solarCalculatorRequest

        <option value="" selected="selected">----- Wybierz -----</option>
                    <option value="18">0 stopni</option>
                    <option value="17">5 stopni</option>
                    <option value="16">10 stopni</option>
                    <option value="15">15 stopni</option>
                    <option value="14">20 stopni</option>
                    <option value="13">25 stopni</option>
                    <option value="12">30 stopni</option>
                    <option value="11">35 stopni</option>
                    <option value="10">40 stopni</option>
                    <option value="9">45 stopni</option>
                    <option value="8">50 stopni</option>
                    <option value="7">55 stopni</option>
                    <option value="6">60 stopni</option>
                    <option value="5">65 stopni</option>
                    <option value="4">70 stopni</option>
                    <option value="3">75 stopni</option>
                    <option value="2">80 stopni</option>
                    <option value="1">85 stopni</option>
                    <option value="0">90 stopni</option>
         */

    }

    private void setRoofAnglePercent(int number) {
        Map<Integer, Integer> percentsMap = new HashMap<Integer, Integer>();
        percentsMap.put(0, 0);
        percentsMap.put(1, 5);
        percentsMap.put(2, 10);
        percentsMap.put(3, 15);
        percentsMap.put(4, 20);
        percentsMap.put(5, 25);
        percentsMap.put(6, 30);
        percentsMap.put(7, 35);
        percentsMap.put(8, 40);
        percentsMap.put(9, 45);
        percentsMap.put(10, 50);
        percentsMap.put(11, 55);
        percentsMap.put(12, 60);
        percentsMap.put(13, 65);
        percentsMap.put(14, 70);
        percentsMap.put(15, 75);
        percentsMap.put(16, 80);
        percentsMap.put(17, 85);
        percentsMap.put(18, 90);

        roofAnglePercent = percentsMap.get(number);

        /* html form from solarCalculatorRequest

        <option value="0">0 stopni (poziomo) </option>
                    <option value="1">5 stopni</option>
                    <option value="2">10 stopni</option>
                    <option value="3">15 stopni</option>
                    <option value="4">20 stopni</option>
                    <option value="5">25 stopni</option>
                    <option value="6">30 stopni</option>
                    <option value="7">35 stopni</option>
                    <option value="8">40 stopni</option>
                    <option value="9">45 stopni</option>
                    <option value="10">50 stopni</option>
                    <option value="11">55 stopni</option>
                    <option value="12">60 stopni</option>
                    <option value="13">65 stopni</option>
                    <option value="14">70 stopni</option>
                    <option value="15">75 stopni</option>
                    <option value="16">80 stopni</option>
                    <option value="17">85 stopni</option>
                    <option value="18">90 stopni (pionowo) </option>
         */

    }

    public int getDeviationFromSouthPercent() {
        return deviationFromSouthPercent;
    }

    public int getRoofAnglePercent() {
        return roofAnglePercent;
    }

    public int getKwh() {
        return kwh;
    }

    public void setKwh(int kwh) {
        this.kwh = kwh;
    }

    public int getBill() {
        return bill;
    }

    public void setBill(int bill) {
        this.bill = bill;
    }

    public int getRoofAngle() {
        return roofAngle;
    }

    public void setRoofAngle(int roofAngle) {
        this.roofAngle = roofAngle;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getDeviationFromSouth() {
        return deviationFromSouth;
    }

    public void setDeviationFromSouth(int deviationFromSouth) {
        this.deviationFromSouth = deviationFromSouth;
    }

    public int getRoofArea() {
        return roofArea;
    }

    public void setRoofArea(int roofArea) {
        this.roofArea = roofArea;
    }

    public String getRoofType() {
        return roofType;
    }
}
