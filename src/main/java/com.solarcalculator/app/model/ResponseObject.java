package com.solarcalculator.app.model;

import java.math.BigDecimal;

/**
 * Created by kornel on 09.07.17.
 */
public class ResponseObject {
    private BigDecimal energyDemandLevel; //how much energy does building needs.
    private BigDecimal areaForDemand; // area for 100% covering energy demand
    private BigDecimal investmentCost; // cost of investment
    private int returnPeriod; // time when system become profitable
    private BigDecimal coverage;
    private PvInfoFromUser pvInfoFromUser;

    public ResponseObject(BigDecimal energyDemandLevel, BigDecimal areaForDemand,
                          BigDecimal investmentCost, int returnPeriod, BigDecimal coverage,
                          PvInfoFromUser pvInfoFromUser) {
        this.energyDemandLevel = energyDemandLevel;
        this.areaForDemand = areaForDemand;
        this.investmentCost = investmentCost;
        this.returnPeriod = returnPeriod;
        this.pvInfoFromUser = pvInfoFromUser;
        this.coverage = coverage;
    }

    public BigDecimal getEnergyDemandLevel() {
        return energyDemandLevel;
    }

    public BigDecimal getAreaForDemand() {
        return areaForDemand;
    }

    public BigDecimal getInvestmentCost() {
        return investmentCost;
    }

    public int getReturnPeriod() {
        return returnPeriod;
    }

    public PvInfoFromUser getPvInfoFromUser() {
        return pvInfoFromUser;
    }

    public BigDecimal getCoverage() {
        return coverage;
    }

}
