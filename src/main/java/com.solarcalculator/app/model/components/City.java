package com.solarcalculator.app.model.components;

/**
 * Created by kornel on 06.07.17.
 */
public class City {
    private String name;    // name of town
    private int insolation; //insolation (naslonecznienie) of town
    private int sunnyHours; //number of sunny hours at town

    public City(String name, int insolation, int sunnyHours) {
        this.name = name;
        this.insolation = insolation;
        this.sunnyHours = sunnyHours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getInsolation() {
        return insolation;
    }

    public void setInsolation(int insolation) {
        this.insolation = insolation;
    }

    public int getSunnyHours() {
        return sunnyHours;
    }

    public void setSunnyHours(int sunnyHours) {
        this.sunnyHours = sunnyHours;
    }
}
