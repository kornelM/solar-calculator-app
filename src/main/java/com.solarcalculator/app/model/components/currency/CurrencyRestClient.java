package com.solarcalculator.app.model.components.currency;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;
import java.math.BigDecimal;

/**
 * Created by kornel on 17.07.17.
 */
public class CurrencyRestClient {
    private Currency euroCurrency;


    public CurrencyRestClient(String url) {
        euroCurrency = getCurrencyNbp(url);
    }

    private Currency getCurrencyNbp(String url) {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);

        HttpResponse response;
        Currency currency = null;
        try {
            response = client.execute(request);
            String content = IOUtils.toString(response.getEntity().getContent(), "UTF-8");

            ObjectMapper objectMapper = new ObjectMapper();

            currency = objectMapper.readValue(content, Currency.class);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return currency;
    }

    public BigDecimal getSellPrice(){
        return euroCurrency.getRates().get(0).getAsk(); //sell price of currency
    }
}
