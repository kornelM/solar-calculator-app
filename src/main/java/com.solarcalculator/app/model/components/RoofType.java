package com.solarcalculator.app.model.components;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kornel on 13.07.17.
 */
public class RoofType {
    private String name;
    private double pricePerWp;
    private Map<String, Double> mapOfPrices;

    public RoofType(String type) {
        initMapOfPrices();
        name = type;
        pricePerWp = mapOfPrices.get(type);
    }

    private void initMapOfPrices() {
        mapOfPrices = new HashMap<String, Double>();
        mapOfPrices.put("Dach skośny - blachodachówka", 0.08);
        mapOfPrices.put("Dach skośny - dachówka", 0.09);
        mapOfPrices.put("Dach skośny – blacha trapezowa", 0.07);
        mapOfPrices.put("Instalacja naziemna", 0.11);
        mapOfPrices.put("Dach płaski", 0.10);
    }

    public double getPricePerWp() {
        return pricePerWp;
    }
}
