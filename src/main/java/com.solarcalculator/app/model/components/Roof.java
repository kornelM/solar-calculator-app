package com.solarcalculator.app.model.components;

import java.math.BigDecimal;

/**
 * Created by kornel on 12.07.17.
 */
public class Roof {
    private int area;
    private RoofType type;

    public Roof(int area, String name) {
        this.area = area;
        type = new RoofType(name);
    }

    public RoofType getType() {
        return type;
    }

    public int getArea() {
        return area;
    }
    }
