package com.solarcalculator.app.model.components;

/**
 * Created by kornel on 12.07.17.
 */
public class Panel {
    private int power;
    private String name;
    private double pricePerWp;

    public Panel(int power, String name, double pricePerWp) {
        this.power = power;
        this.name = name;
        this.pricePerWp = pricePerWp;
    }

    public int getPower() {
        return power;
    }

    public String getName() {
        return name;
    }

    public double getPricePerWp() {
        return pricePerWp;
    }
}
