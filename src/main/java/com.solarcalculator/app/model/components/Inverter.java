package com.solarcalculator.app.model.components;

/**
 * Created by kornel on 17.07.17.
 */
public class Inverter {
    private String name;
    private int power;
    private int price;// euro

    public Inverter(String name, int power, int price) {
        this.name = name;
        this.power = power;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public int getPower() {
        return power;
    }

    public int getPrice() {
        return price;
    }
}
