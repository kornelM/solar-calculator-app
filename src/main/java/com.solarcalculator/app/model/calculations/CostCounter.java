package com.solarcalculator.app.model.calculations;

import com.solarcalculator.app.model.components.Inverter;
import com.solarcalculator.app.model.components.Panel;
import com.solarcalculator.app.model.components.Roof;
import com.solarcalculator.app.model.components.currency.CurrencyRestClient;
import com.solarcalculator.app.repositories.PowerInverterRepository;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by kornel on 16.07.17.
 */
public class CostCounter {
    private final String EURO = "http://api.nbp.pl/api/exchangerates/rates/c/eur/today";

    private Panel panelPv;
    private Roof roof;
    private BigDecimal totalCostOfSystem;
    private int numOfPanels;
    private double energyDemand;
    private PowerInverterRepository inverterRepository;
    private Inverter inverter;
    private CurrencyRestClient currencyRestClient;

    public CostCounter(AreaCounter areaCounter, double energyDemand) {
        panelPv = areaCounter.getPanelPv();
        roof = areaCounter.getRoof();
        numOfPanels = areaCounter.getNumOfPanels();
        this.energyDemand = energyDemand;
        inverterRepository = new PowerInverterRepository();
        currencyRestClient = new CurrencyRestClient(EURO);
        countTotalCost();
    }

    private void findRightInverter() {
        //list has to be sorted!!!

        for (int i = 0; i < inverterRepository.getListOfInverters().size(); i++) {
            if (energyDemand < inverterRepository.getListOfInverters().get(i).getPower()) {
                inverter = inverterRepository.getListOfInverters().get(i);
                break;
            } else {
                inverter = inverterRepository.getListOfInverters().get(i);
                break;
            }
        }
    }


    private void countTotalCost() {
        BigDecimal tempCost = BigDecimal.valueOf(numOfPanels * panelPv.getPricePerWp() * panelPv.getPower());
        tempCost =tempCost.add(BigDecimal.valueOf(roof.getType().getPricePerWp() * energyDemand * 1000));
        findRightInverter();
        tempCost = tempCost.add(BigDecimal.valueOf(inverter.getPrice()));
        tempCost = tempCost.multiply(currencyRestClient.getSellPrice());


        totalCostOfSystem = tempCost.setScale(0, BigDecimal.ROUND_HALF_UP);
    }

    public BigDecimal getTotalCostOfSystem() {
        return totalCostOfSystem;
    }
}
