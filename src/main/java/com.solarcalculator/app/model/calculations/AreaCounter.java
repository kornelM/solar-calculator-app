package com.solarcalculator.app.model.calculations;

import com.solarcalculator.app.model.PvInfoFromUser;
import com.solarcalculator.app.model.components.Panel;
import com.solarcalculator.app.model.components.Roof;
import com.solarcalculator.app.repositories.PanelsRepository;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by kornel on 12.07.17.
 */
public class AreaCounter {
    private final double AREA_OF_PANEL = 1.7;

    private PanelsRepository panelsRepository;
    private Panel panelPv;
    private Roof roof;
    private BigDecimal energyDemand;
    private BigDecimal areaDemand;
    private BigDecimal coverage;
    private int numOfPanels;

    public AreaCounter(PvInfoFromUser pvInfoFromUser, BigDecimal energyDemand) {
        panelsRepository = new PanelsRepository();
        roof = new Roof(pvInfoFromUser.getRoofArea(), pvInfoFromUser.getRoofType());
        this.energyDemand = energyDemand;
        count(pvInfoFromUser.getKwh());
        coveredEnergyDemandFromUsersRoof();
    }

    private void count(int energyDemand) {
        List<Panel> listOfPanels = panelsRepository.getListOfPanels();

        double countedAreaOfPanels;

        for (int i = 0; i < listOfPanels.size(); i++) {
            countedAreaOfPanels = countAreaOfPanels(energyDemand, listOfPanels.get(i));
            if (roof.getArea() > countedAreaOfPanels) {
                panelPv = listOfPanels.get(i);
                areaDemand = BigDecimal.valueOf(countedAreaOfPanels).setScale(2, BigDecimal.ROUND_HALF_UP);
                break;
            } else {
                panelPv = panelsRepository.getTheStrongest();
                areaDemand = BigDecimal.valueOf(countAreaOfPanels(energyDemand, panelPv)).setScale(2, BigDecimal.ROUND_HALF_UP);
            }
        }
    }

    private double countAreaOfPanels(int energyDemand, Panel panel) {
        numOfPanels = Math.round(energyDemand /panel.getPower());
        return numOfPanels * AREA_OF_PANEL;
    }


    public void coveredEnergyDemandFromUsersRoof() {
        double temp = Double.valueOf(roof.getArea()) / Double.valueOf(String.valueOf(areaDemand)) * 100;
        if (temp < 100)
            coverage = new BigDecimal(temp).setScale(2, BigDecimal.ROUND_HALF_UP);
        else
            coverage = new BigDecimal(100);
    }

    public Panel getPanelPv() {
        return panelPv;
    }

    public Roof getRoof() {
        return roof;
    }

    public int getNumOfPanels() {
        return numOfPanels;
    }

    public String getName() {
        return panelPv.getName();
    }

    public BigDecimal getNeededArea() {
        return areaDemand;
    }

    public BigDecimal getCoverage() {
        return coverage;
    }
}