package com.solarcalculator.app.model.calculations;

import com.solarcalculator.app.model.PvInfoFromUser;
import com.solarcalculator.app.repositories.CitiesRepository;

import java.math.BigDecimal;
import java.text.DecimalFormat;

/**
 * Created by kornel on 07.07.17.
 */
public class EnergyCounter {
    private PvInfoFromUser pvInfoFromUser;
    private CorrectionFactor correctionFactor;
    private Double energyDemand;
    private CitiesRepository citiesRepository;

    public EnergyCounter(PvInfoFromUser pvInfoFromUser) {
        this.pvInfoFromUser = pvInfoFromUser;
        citiesRepository = new CitiesRepository();
        correctionFactor = new CorrectionFactor();
        countEnergyDemand();
    }

    public BigDecimal countEnergyDemand() {
        Double firstEnergyFormula = firstEnergyDemandFormula();
        Double secondEnergyFormula = secondEnergyDemandFormula();


        energyDemand = averageEnergyDemand(firstEnergyFormula, secondEnergyFormula);
        BigDecimal energyDemandDecimal =  BigDecimal.valueOf(energyDemand).setScale(2, BigDecimal.ROUND_HALF_UP);

        return energyDemandDecimal;
    }

    private Double firstEnergyDemandFormula() {
        int kwh = pvInfoFromUser.getKwh();
        int insolation = citiesRepository.getCityByName(pvInfoFromUser.getCity()).getInsolation();
        Double factor = correctionFactor.getCorrectionFactor(
                pvInfoFromUser.getRoofAngle(),
                pvInfoFromUser.getDeviationFromSouth()); //correction factor
        Double efficiency = 0.8;
        int iOr = 1; // intensity of radiation

        Double result = kwh * iOr / (insolation * factor * efficiency);

        return result;
    }

    public BigDecimal getEnergyDemand() {
        return new BigDecimal(energyDemand).setScale(2, BigDecimal.ROUND_HALF_UP);
    }

    public double getEneDemand() {
        return energyDemand;
    }

    private Double secondEnergyDemandFormula() {
        int kwh = pvInfoFromUser.getKwh();
        int insolation = citiesRepository.getCityByName(pvInfoFromUser.getCity()).getInsolation();
        Double constant = 0.9;
        Double efficiency = 0.8;

        Double result = constant * kwh / (insolation * efficiency);

        return result;
    }

    private Double averageEnergyDemand(Double first, Double second) {
        return (first + second) / 2;
    }
}
