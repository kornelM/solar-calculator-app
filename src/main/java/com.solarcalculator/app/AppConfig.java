package com.solarcalculator.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by kornel on 06.07.17.
 */




@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan
public class AppConfig {
    public static void main(String[] args) {

        SpringApplication.run(AppConfig.class, args);
    }
}
