package com.solarcalculator.app.repositories;

import com.solarcalculator.app.model.components.Panel;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kornel on 12.07.17.
 */
public class PanelsRepository {
    private List<Panel> listOfPanels;


    public PanelsRepository() {
        initListOfPanels();
    }

    private void initListOfPanels() {
        listOfPanels = new ArrayList<Panel>();
        listOfPanels.add(new Panel(250, "SE 250/60P", 0.52)); //have to add panels to list
        listOfPanels.add(new Panel(260, "SE 260/60P", 0.53)); //have to add panels to list
        listOfPanels.add(new Panel(270, "SE 270/60P", 0.54)); //have to add panels to list
        listOfPanels.add(new Panel(280, "SE 280/60P", 0.56)); //have to add panels to list
        listOfPanels.add(new Panel(270, "SE 270/60M", 0.53)); //have to add panels to list
        listOfPanels.add(new Panel(280, "SE 280/60M", 0.53)); //have to add panels to list
        listOfPanels.add(new Panel(290, "SE 290/60M", 0.55)); //have to add panels to list
        listOfPanels.add(new Panel(300, "SE 300/60M", 0.57)); //have to add panels to list
        listOfPanels.add(new Panel(310, "SE 310/72P", 0.55)); //have to add panels to list
        listOfPanels.add(new Panel(315, "SE 315/72P", 0.56)); //have to add panels to list
        listOfPanels.add(new Panel(320, "SE 320/72P", 0.57)); //have to add panels to list
        listOfPanels.add(new Panel(325, "SE 325/72P", 0.58)); //have to add panels to list
        listOfPanels.add(new Panel(320, "SE 320/72M", 0.55)); //have to add panels to list
        listOfPanels.add(new Panel(320, "SE 320/72M", 0.56)); //have to add panels to list
        listOfPanels.add(new Panel(320, "SE 320/72M", 0.57)); //have to add panels to list
        listOfPanels.add(new Panel(320, "SE 320/72M", 0.59)); //have to add panels to list
    }

    public Panel mostExpensive() {
        Panel mostExpensivePanel = listOfPanels.get(0);
        for (int i = 1; i < listOfPanels.size(); i++) {
            if (listOfPanels.get(i).getPricePerWp() > mostExpensivePanel.getPricePerWp())
                mostExpensivePanel = listOfPanels.get(i);
        }
        return mostExpensivePanel;
    }

    public Panel theCheapest() {
        Panel theCheapestPanel = listOfPanels.get(0);
        for (int i = 1; i < listOfPanels.size(); i++) {
            if (listOfPanels.get(i).getPricePerWp() > theCheapestPanel.getPricePerWp())
                theCheapestPanel = listOfPanels.get(i);
        }
        return theCheapestPanel;
    }

    public Panel getTheStrongest() {
        Panel theStrongestPanel = listOfPanels.get(0);
        for (int i = 1; i < listOfPanels.size(); i++) {
            if (listOfPanels.get(i).getPower() > theStrongestPanel.getPower())
                theStrongestPanel = listOfPanels.get(i);
        }
        return theStrongestPanel;
    }

    public Panel getTheWeakest(){
        Panel theWeakestPanel = listOfPanels.get(0);
        for (int i = 1; i < listOfPanels.size(); i++) {
            if (listOfPanels.get(i).getPower() > theWeakestPanel.getPower())
                theWeakestPanel = listOfPanels.get(i);
        }
        return theWeakestPanel;
    }


    public List<Panel> getListOfPanels() {
        return listOfPanels;
    }
}
