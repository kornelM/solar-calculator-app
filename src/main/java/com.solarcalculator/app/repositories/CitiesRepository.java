package com.solarcalculator.app.repositories;

import com.solarcalculator.app.model.components.City;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by kornel on 06.07.17.
 */
public class CitiesRepository {
    private Map<String, City> mapOfPolishCities;

    public CitiesRepository() {
        mapOfPolishCities = new HashMap<String, City>();
        mapOfPolishCities.put("Szczecin", new City("Szczecin", 1056, 1550));
        mapOfPolishCities.put("Koszalin", new City("Koszalin", 1062, 1620));
        mapOfPolishCities.put("Wałcz", new City("Wałcz", 1039, 1610));
        mapOfPolishCities.put("Gdańsk", new City("Gdańsk", 1065, 1630));
        mapOfPolishCities.put("Słupsk", new City("Słupsk", 1070, 1500));
        mapOfPolishCities.put("Chojnice", new City("Chojnice", 1077, 1460));
        mapOfPolishCities.put("Olsztyn", new City("Olsztyn", 1032, 1580));
        mapOfPolishCities.put("Elbląg", new City("Elbląg", 1067, 1580));
        mapOfPolishCities.put("Ełk", new City("Ełk", 1027, 1590));
        mapOfPolishCities.put("Suwałki", new City("Suwałki", 1004, 1640));
        mapOfPolishCities.put("Białystok", new City("Białystok", 1020, 1630));
        mapOfPolishCities.put("Łomża", new City("Łomża", 1032, 1510));
        mapOfPolishCities.put("Gorzów Wielkopolski", new City("Gorzów Wielkopolski", 1065, 1520));
        mapOfPolishCities.put("Zielona Góra", new City("Zielona Góra", 1053, 1450));
        mapOfPolishCities.put("Żary", new City("Żary", 1030, 1460));
        mapOfPolishCities.put("Piła", new City("Piła", 1043, 1660));
        mapOfPolishCities.put("Poznań", new City("Poznań", 1050, 1590));
        mapOfPolishCities.put("Kalisz", new City("Kalisz", 1045, 1570));
        mapOfPolishCities.put("Bydgoszcz", new City("Bydgoszcz", 1033, 1490));
        mapOfPolishCities.put("Włocławek", new City("Włocławek", 1032, 1540));
        mapOfPolishCities.put("Grudziądz", new City("Grudziądz", 1041, 1520));
        mapOfPolishCities.put("Warszawa", new City("Warszawa", 1038, 1440));
        mapOfPolishCities.put("Radom", new City("Radom", 1085, 1540));
        mapOfPolishCities.put("Ostrołęka", new City("Ostrołęka", 1066, 1500));
        mapOfPolishCities.put("Lublin", new City("Lublin", 1042, 1530));
        mapOfPolishCities.put("Biała Podlaska", new City("Biała Podlaska", 1080, 1540));
        mapOfPolishCities.put("Zamość", new City("Zamość", 1062, 1560));
        mapOfPolishCities.put("Kłodzko", new City("Kłodzko", 1045, 1580));
        mapOfPolishCities.put("Wrocław", new City("Wrocław", 1067, 1580));
        mapOfPolishCities.put("Bolesławiec", new City("Bolesławiec", 1039, 1550));
        mapOfPolishCities.put("Wieluń", new City("Wieluń", 1058, 1590));
        mapOfPolishCities.put("Łódź", new City("Łódź", 1067, 1580));
        mapOfPolishCities.put("Marianka", new City("Marianka", 1070, 1580));
        mapOfPolishCities.put("Sulejów", new City("Sulejów", 1053, 1580));
        mapOfPolishCities.put("Stalowa Wola", new City("Stalowa Wola", 1067, 1520));
        mapOfPolishCities.put("Rzeszów", new City("Rzeszów", 1037, 1500));
        mapOfPolishCities.put("Sanok", new City("Sanok", 1057, 1510));
        mapOfPolishCities.put("Kielce", new City("Kielce", 1057, 1520));
        mapOfPolishCities.put("Busko-Zdrój", new City("Busko-Zdrój", 1047, 1510));
        mapOfPolishCities.put("Końskie", new City("Końskie", 1066, 1530));
        mapOfPolishCities.put("Kraków", new City("Kraków", 1044, 1470));
        mapOfPolishCities.put("Zakopane", new City("Zakopane", 1055, 1430));
        mapOfPolishCities.put("Nowy Sącz", new City("Nowy Sącz", 1110, 1430));
        mapOfPolishCities.put("Tarnów", new City("Tarnów", 1068, 1470));
        mapOfPolishCities.put("Częstochowa", new City("Częstochowa", 1033, 1490));
        mapOfPolishCities.put("Katowice", new City("Katowice", 1009, 1470));
        mapOfPolishCities.put("Bielsko-Biała", new City("Bielsko-Biała", 1046, 1440));
        mapOfPolishCities.put("Rybnik", new City("Rybnik", 1032, 1430));
        mapOfPolishCities.put("Nysa", new City("Nysa", 1070, 1540));
        mapOfPolishCities.put("Opole", new City("Opole", 1043, 1520));
        mapOfPolishCities.put("Kluczbork", new City("Kluczbork", 1045, 1530));
        mapOfPolishCities.put("Nawojowa Góra", new City("Nawojowa Góra", 1031, 1560));
        mapOfPolishCities.put("Witanowice", new City("Witanowice", 1037, 1510));
    }

    public City getCityByName(String cityName){
        return mapOfPolishCities.get(cityName);
    }
}
