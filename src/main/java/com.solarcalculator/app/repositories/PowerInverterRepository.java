package com.solarcalculator.app.repositories;

import com.solarcalculator.app.model.components.Inverter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kornel on 17.07.17.
 */
public class PowerInverterRepository {
    private List<Inverter> listOfInverters;

    public PowerInverterRepository(){
        initListOfInverters();
    }

    private void initListOfInverters(){
        listOfInverters = new ArrayList();
        listOfInverters.add(new Inverter("QY 3000", 3000, 1500)); //price in euro
        listOfInverters.add(new Inverter("QX 4200", 4200, 1397));
        listOfInverters.add(new Inverter("QX 4000", 4000, 2207));
        listOfInverters.add(new Inverter("QX 5000", 5000, 2264));
        listOfInverters.add(new Inverter("QX 6000", 6000, 2504));
        listOfInverters.add(new Inverter("QX 7000", 7000, 2672));
        listOfInverters.add(new Inverter("QX 8000", 8000, 2876));
        listOfInverters.add(new Inverter("QX 10000", 10000, 3116));
        listOfInverters.add(new Inverter("QX 13000", 13000, 3488));
        listOfInverters.add(new Inverter("QX 15000", 15000, 3710));
        listOfInverters.add(new Inverter("QX 18000", 18000, 4062));
    }

    public List<Inverter> getListOfInverters() {
        return listOfInverters;
    }

    public Inverter mostExpensive() {
        Inverter mostExpensiveInverter = listOfInverters.get(0);
        for (int i = 1; i < listOfInverters.size(); i++) {
            if (listOfInverters.get(i).getPrice() > mostExpensiveInverter.getPrice())
                mostExpensiveInverter = listOfInverters.get(i);
        }
        return mostExpensiveInverter;
    }

    public Inverter theCheapest() {
        Inverter theCheapestInverter = listOfInverters.get(0);
        for (int i = 1; i < listOfInverters.size(); i++) {
            if (listOfInverters.get(i).getPrice() > theCheapestInverter.getPrice())
                theCheapestInverter = listOfInverters.get(i);
        }
        return theCheapestInverter;
    }

    public Inverter getTheStrongest() {
        Inverter theStrongestInverter = listOfInverters.get(0);
        for (int i = 1; i < listOfInverters.size(); i++) {
            if (listOfInverters.get(i).getPower() > theStrongestInverter.getPower())
                theStrongestInverter = listOfInverters.get(i);
        }
        return theStrongestInverter;
    }

    public Inverter getTheWeakest(){
        Inverter theWeakestInverter = listOfInverters.get(0);
        for (int i = 1; i < listOfInverters.size(); i++) {
            if (listOfInverters.get(i).getPower() > theWeakestInverter.getPower())
                theWeakestInverter = listOfInverters.get(i);
        }
        return theWeakestInverter;
    }


}
