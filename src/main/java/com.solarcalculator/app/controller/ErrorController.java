package com.solarcalculator.app.controller;

import org.springframework.stereotype.Controller;

import javax.websocket.OnError;

/**
 * Created by kornel on 16.07.17.
 */

@Controller
public class ErrorController {

    @OnError
    public String showErrorPage() {
        return "errorPage";
    }
}
