package com.solarcalculator.app.controller;

import com.solarcalculator.app.model.PvInfoFromUser;
import com.solarcalculator.app.model.ResponseObject;
import com.solarcalculator.app.model.calculations.AreaCounter;
import com.solarcalculator.app.model.calculations.CostCounter;
import com.solarcalculator.app.model.calculations.EnergyCounter;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by kornel on 09.07.17.
 */

@Controller
public class EnergyDemandController {

    private ResponseObject responseObject;
    private EnergyCounter energyCounter;
    private PvInfoFromUser pvInfoFromUser;
    private AreaCounter areaCounter;
private CostCounter costCounter;




    @RequestMapping("/")
    public String showPvCells() {
        return "solarCalculatorRequest";
    }

    @RequestMapping(method = RequestMethod.POST, value = "/submit") // have to change it
    public ModelAndView showResults(
            @RequestParam("kwh") int kwh,               // average annual usage of energy
            @RequestParam("electricityBill") int bill,  // average annual bill for electricity
            @RequestParam("roofAngle") int roofAngle,   //angle of roof
            @RequestParam("city") String city,          // name of city (because of amount of energy from Sun)
            @RequestParam("direction") int deviationFromSouth,
            @RequestParam("area") int roofArea,
            @RequestParam("roofType") String roofType,
            ModelAndView modelAndView) {

        pvInfoFromUser = new PvInfoFromUser(kwh, bill, roofAngle, city, deviationFromSouth, roofArea, roofType);
        energyCounter = new EnergyCounter(pvInfoFromUser);
        areaCounter = new AreaCounter(pvInfoFromUser, energyCounter.getEnergyDemand());
        costCounter = new CostCounter(areaCounter, energyCounter.getEneDemand());
        responseObject = new ResponseObject(energyCounter.getEnergyDemand(), areaCounter.getNeededArea(), costCounter.getTotalCostOfSystem(), 12, areaCounter.getCoverage(), pvInfoFromUser);




        modelAndView.addObject("response", responseObject);

        modelAndView.setViewName("solarCalculatorResponse");

        return modelAndView;
    }

}
